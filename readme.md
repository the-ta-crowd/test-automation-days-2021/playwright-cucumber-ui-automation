## Setup

run `npm i`

## Commands

Run tests in headed mode: `npm run test`

Run tests in headless mode: `npm run test:headless`

## Preperation for assignment @ TA Days:

1. Delete css selectors from page-objects
2. Delete contents of methods in page-objects (or perhaps delete methods as well?)

(so leave the Gherkin file and stepdefs intact to speed up things for the assignee, or not, your choice ;)