Feature: Add movie to overview

  Scenario: Add movie to overview
  Given an admin is logged in
  When the user creates a new movie
  Then this movie is visible in the movie overview
