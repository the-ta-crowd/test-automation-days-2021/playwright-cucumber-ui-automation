import {After, Before, Given, setWorldConstructor, Then, When} from '@cucumber/cucumber';

import {WorldWithPlaywright} from '../support/world-with-playwright';
import {LoginPage} from "../support/page-objects/login.page";
import {ProductsPage} from "../support/page-objects/products.page";

setWorldConstructor(WorldWithPlaywright);

Before(async function (this: WorldWithPlaywright) {
    await this.setupWorldWithPage();
});

Given(/^an admin is logged in$/, async function (this: WorldWithPlaywright) {
    const loginPage = new LoginPage(this.page);
    await loginPage.goto();
    await loginPage.login('admin', 'admin');
});
When(/^the user creates a new movie$/, async function (this: WorldWithPlaywright) {
    const productsPage = new ProductsPage(this.page);
    await productsPage.goto();
    await productsPage.registerProduct('Inception');
});

Then(/^this movie is visible in the movie overview$/, async function (this: WorldWithPlaywright) {
    const productsPage = new ProductsPage(this.page);
    await productsPage.goto();
    await productsPage.checkProductInOverview('Inception');
});

After(async function (this: WorldWithPlaywright) {
    await this.closeBrowser();
});