import { IWorldOptions, World } from '@cucumber/cucumber';
import { Page } from '@playwright/test';
import { chromium } from 'playwright';

interface WorldWithPlaywrightParameters {
  headed: boolean;
}

export class WorldWithPlaywright extends World<WorldWithPlaywrightParameters> {

  page!: Page;

  private browser: any;

  constructor(options: IWorldOptions<any>) {
    super(options);
  }

  async setupWorldWithPage() {
    this.browser = await chromium.launch({
      headless: !this.parameters.headed,
      slowMo: 200
    });
    const context = await this.browser.newContext();
    this.page = await context.newPage();
  }

  async closeBrowser() {
    await this.browser.close();
  }
}
