import {expect, Page} from '@playwright/test';

export class ProductsPage {

    private readonly page: Page;

    private readonly url = 'http://localhost:8080/product';

    private readonly editBtn = '.menu__edit-button';
    private readonly createNewBtn = 'input[value="Create new"]'

    private readonly inputTitle = 'input[name=title]';
    private readonly inputReleaseYear = 'input[name=releaseYear]';
    private readonly inputGenreAction = '#genre_1';
    private readonly inputDescription = 'textarea[name=description]';
    private readonly inputOfficialWebsite = 'input[name=website]';
    private readonly inputMoviePoster = 'input[name=poster]';
    private readonly submitButton = '[type=submit]';

    constructor(page: Page) {
        this.page = page;
    }

    async goto() {
        await this.page.goto(this.url);
    }

    async registerProduct(title: string) {
        await this.page.click(this.editBtn);
        await this.page.click(this.createNewBtn);
        await this.page.fill(this.inputTitle, title);
        await this.page.fill(this.inputReleaseYear, '2010');
        await this.page.click(this.inputGenreAction);
        await this.page.fill(this.inputDescription, 'Movie about dreams');
        await this.page.fill(this.inputOfficialWebsite, 'https://en.wikipedia.org/wiki/Inception');
        await this.page.setInputFiles(this.inputMoviePoster, `./resources/inception.png`);
        await this.page.click(this.submitButton);
    }

    async checkProductInOverview(title: string) {
        console.log(`Validate movie '${title}' in overview...`);
        await expect(this.page.locator(`a:has-text("${title}")`)).toBeTruthy();
        console.log(`Validated '${title}'`);
    }
}
