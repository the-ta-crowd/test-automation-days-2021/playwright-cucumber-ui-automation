import {expect, Page} from '@playwright/test';

export class LoginPage {

  private readonly page: Page;

  private readonly url = 'http://localhost:8080/users/login';

  private readonly inputUsername = 'input[name=username]';
  private readonly inputPassword = 'input[name=password]';
  private readonly btnSubmit = 'button[value=Submit]'

  constructor(page: Page) {
    this.page = page;
  }

  async goto() {
    await this.page.goto(this.url);
  }

  async login(username: string, password: string) {
    await this.page.fill(this.inputUsername, username);
    await this.page.fill(this.inputPassword, password);
    await this.page.click(this.btnSubmit);
    await expect(this.page.locator('main > h1')).toHaveText('Welcome to the Capgemini Movie Store');
  }
}
