const defaults = {
    paths: ['tests/**/*.feature'],
    requireModule: ['ts-node/register'],
    require: ['tests/**/*.ts'],
    publishQuiet: true,
    parallel: 1,
    format: ['summary'],
    worldParameters: {
        headed: false,
    },
};

module.exports = {
    default: {
        ...defaults,
    },
    headed: {
        ...defaults,
        worldParameters: {
            headed: true,
        },
    },
};


